
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
<link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap.min.css" rel="stylesheet">
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/plug-ins/f2c75b7247b/integration/bootstrap/3/dataTables.bootstrap.css">
<script src="http://cdn.datatables.net/1.10.5/js/jquery.dataTables.min.js"></script>
<script src="http://cdn.datatables.net/plug-ins/f2c75b7247b/integration/bootstrap/3/dataTables.bootstrap.js" ></script>

<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/js/bootstrap.min.js"></script>
<script>
	
	var oTable = null;

	$(document).ready(function(){	

	oTable = $('#reportTable').dataTable({

		// "ajax":'allBlogs',
		"sAjaxSource":'reportdata',
		"bProcessing": true,
		"bServerSide": true,
		"dom": 'T<"clear">lfrtip',
        "tableTools": {
            "sSwfPath": "/swf/copy_csv_xls_pdf.swf"
        },
		"aoColumns": [
			{ "mDataProp" : "full_message"},
			{ "mDataProp" : "campaign_id"},
	        { "mDataProp" : "customer_name"},
	        { "mDataProp" : "age"},
	        { "mDataProp" : "gender"},
	        { "mDataProp" : "customer_mobile"},
	        { "mDataProp" : "thana"},
	        { "mDataProp" : "district"},
	        { "mDataProp" : "division"},
	        { "mDataProp" : "education_id"},
	        { "mDataProp" : "occupation_id"},
	        { "mDataProp" : "bp_name"},
	        { "mDataProp" : "bp_mobile"},
	        { "mDataProp" : "lsa_code"},
	        { "mDataProp" : "coupon_code"},
	        { "mDataProp" : "currently_used_product"},
	        { "mDataProp" : "sales"},
	        { "mDataProp" : "FAL"},
	        { "mDataProp" : "PDF"},
	        { "mDataProp" : "PWB"},
	        { "mDataProp" : "PNS"},
	        { "mDataProp" : "PPC"},
	        { "mDataProp" : "DBM"},
	        { "mDataProp" : "date"},
	        { "mDataProp" : "error"}
        ],           
    
		// "lengthMenu": [[5,10,15,-1],[5,10,15,"All"]]		
	});
});
	
</script>


<div class="table-responsive col-lg-8">

	<table id="reportTable" class="table table-condensed table-hover" cellspacing="0" width="100%">
		
		<thead>
			<tr>
				<td>Full Message</td>
				<td>Campaign ID</td>
				<td>Name</td>
				<td>Age</td>
				<td>Gender</td>
				<td>Mobile</td>
				<td>Thana</td>
				<td>District</td>
				<td>Division</td>
				<td>Education</td>
				<td>Occupation</td>
				<td>BP Name</td>
				<td>BP Mobile</td>
				<td>LSA Code</td>
				<td>Coupon</td>				
				<td>Used Product</td>
				<td>Sales Status</td>
				<td>FAL</td>
				<td>PDF</td>
				<td>PWB</td>
				<td>PNS</td>
				<td>PPC</td>
				<td>DBM</td>
				<td>Date</td>
				<td>ERROR</td>
				
			</tr>
		</thead>

		<tbody>
			<tr>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				
			</tr>
		</tbody>


	</table>

</div>








