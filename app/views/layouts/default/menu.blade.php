<!--Main Menu-->
<div class="responsive-admin-menu">
	<div class="responsive-menu">
		<div class="menuicon"><i class="fa fa-angle-down"></i></div>
	</div>
	<ul id="menu">

		<li><a class="active" href="{{URL::to('/')}}" title="Dashboard"><i class="entypo-briefcase"></i><span> Dashboard</span></a></li>
    
	
		<li> <a class="submenu" href="#" data-id="pages-sub" title="Pages"><i class="entypo-book"></i><span> U1</span></a>
			<ul id="pages-sub" class="accordion">
				<li>{{link_to('ytd/u1','YTD Report')}}</li>
				<li>{{link_to('monthwise/u1','Month Wise Summary')}}</li>
				<li>{{link_to('areawise/u1','Area Wise Summary')}}</li>
				<li>{{link_to('u1table','Full Report')}}</li>
			</ul>
	</li>
	</ul>
</div>
<!--/MainMenu-->