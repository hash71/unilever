<!--Scripts--> 
<!--JQuery--> 

{{HTML::script('assets/js/vendors/jquery/jquery.min.js')}}
{{HTML::script('assets/js/vendors/jquery/jquery-ui.min.js')}}
 

<script>
$('.powerwidget > header').on('touchstart', function(event){});
</script>

<!--EasyPieChart--> 

{{HTML::script('assets/js/vendors/easing/jquery.easing.1.3.min.js')}}
{{HTML::script('assets/js/vendors/easypie/jquery.easypiechart.min.js')}}


<!--Fullscreen--> 

{{HTML::script('assets/js/vendors/fullscreen/screenfull.min.js')}}

<!--Sparkline--> 
{{HTML::script('assets/js/vendors/sparkline/jquery.sparkline.min.js')}}



<!--Horizontal Dropdown--> 

{{HTML::script('assets/js/vendors/horisontal/cbpHorizontalSlideOutMenu.js')}}
{{HTML::script('assets/js/vendors/classie/classie.js')}}

<!--PowerWidgets--> 

{{HTML::script('assets/js/vendors/powerwidgets/powerwidgets.min.js')}}


<!--Morris Chart--> 
{{HTML::script('assets/js/vendors/raphael/raphael-min.js')}}
{{HTML::script('assets/js/vendors/morris/morris.min.js')}}


<!--FlotChart--> 
{{HTML::script('assets/js/vendors/flotchart/jquery.flot.min.js')}}
{{HTML::script('assets/js/vendors/flotchart/jquery.flot.resize.min.js')}}
{{HTML::script('assets/js/vendors/flotchart/jquery.flot.axislabels.js')}}
{{HTML::script('assets/js/vendors/flotchart/jquery.flot.pie.min.js')}}


<!--Chart.js--> 
{{HTML::script('assets/js/vendors/chartjs/chart.min.js')}}





<!--Bootstrap--> 

{{HTML::script('assets/js/vendors/bootstrap/bootstrap.min.js')}}

<!--ToDo-->
{{HTML::script('assets/js/vendors/todos/todos.js')}}


<!--Main App--> 

{{HTML::script('assets/js/scripts.js')}}

<script type="text/javascript">
            // When the document is ready
            $(document).ready(function () {
                
                $('#datetimepicker1').datepicker({
                    format: "dd/mm/yyyy"
                });  
            
            });
</script>




