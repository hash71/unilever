<!DOCTYPE html>

@include('layouts.default.metaHead')

<body>

<!--Smooth Scroll-->

<div class="smooth-overflow">

@include('layouts.default.navigation')
  
  <!--MainWrapper-->
  <div class="main-wrap">  
   
    @include('layouts.default.menu')
    
    @yield('content')
    
  </div>
  <!--/MainWrapper-->
</div>
<!--/Smooth Scroll-->

@include('layouts.default.modals')

@include('layouts.default.scripts')

@yield('u1tableJS')

</body>
</html>