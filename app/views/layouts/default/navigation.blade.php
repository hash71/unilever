<!--Navigation-->
<nav class="main-header clearfix" role="navigation"> <a class="navbar-brand" href="{{URL::to('/')}}">{{HTML::image('assets/images/logo1.png')}}</a> 

  
  <!--Search-->
  <div class="site-search">
    <form action="#" id="inline-search">
      <i class="fa fa-search"></i>
      <input type="search" placeholder="Search">
    </form>
  </div>
  
  <!--Navigation Itself-->    
  <div class="navbar-content">

    <!--Sidebar Toggler--> 
    <a href="#" class="btn btn-default left-toggler"><i class="fa fa-bars"></i></a>

    <!--Fullscreen Trigger-->
    <button type="button" class="btn btn-default hidden-xs pull-right" id="toggle-fullscreen"><i class=" entypo-popup"></i></button>
    
    <!--Lock Screen--> 
    <a href="#" data-toggle="modal" class="btn btn-default hidden-xs pull-right lockme"> <i class="entypo-lock"></i> </a>     

  </div>
</nav>  
<!--/Navigation--> 