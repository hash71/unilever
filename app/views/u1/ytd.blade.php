@extends('layouts.default.master')
@section('content')
  <div class="content-wrapper">
        <!-- 
        Please Dont Delete This portion  <nav class="cbp-hsmenu-wrapper" id="cbp-hsmenu-wrapper"></nav>  
        -->
        <nav class="cbp-hsmenu-wrapper" id="cbp-hsmenu-wrapper"></nav>

        <!--Breadcrumb-->
        <div class="breadcrumb clearfix">
          <ul>
            <li><a href="{{URL::to('/')}}"><i class="fa fa-home"></i></a></li>
            <li><a href="{{URL::to('/')}}">Dashboard</a></li>
            <li class="active">YTD Report</li>
          </ul>
        </div>
        <!--/Breadcrumb--> 
        
          
        <!-- Widget Row Start grid -->
        <div class="row" id="powerwidgets">
                          
          <div class="col-md-12 bootstrap-grid"> 
            
          <!-- YTD Table -->           
            <div class="powerwidget cold-grey" id="YTD" data-widget-editbutton="false">
              <header>
                <h2>YTD Report of Face Wash D2D Activation 2015</small></h2>
              </header>
              <div class="inner-spacer">
                <div class="cus-table">
                  <div class="table-responsive">
                    <table class="table table-condensed table-bordered table-hover margin-0px">
                      <thead>
                        <tr>
                          <th>Summary  Report</th>
                          <th colspan="5">House Holds Status</th>
                          <th colspan="5">Sales Status</th>
                          <th colspan="6">Todays Status </th>
                        </tr>
                      </thead>
                      <tbody>
                        <tr>
                          <td><b>SKUs</b></td>
                          <td><b>HH Target</b></td>
                          <td><b>YTD HH Target</b></td>
                          <td><b>Actual visits</b></td>
                          <td><b>% of Total Target</b></td>
                          <td><b>% of YTD</b></td>

                          <td><b>Total Sales Target</b></td>
                          <td><b>YTD Sales Target</b></td>
                          <td><b>YTD Products sold</b></td>
                          <td><b>YTD Achieved % </b></td>
                          <td><b>YTD Conversion  %</b></td>


                          <td><b>HH Target</b></td>
                          <td><b>Actual visits</b></td>
                          <td><b>% of Target</b></td>
                          <td><b>Sales Target</b></td>
                          <td><b>Sales Achieved</b></td>
                          <td><b>% of Sales</b></td>
                        </tr>
                        <tr>
                          <td>{{$datas['camp']}}</td>
                          <td>{{$datas['hh_target']}}</td>
                          <td>{{$datas['ytd_hh_target']}}</td>
                          <td>{{$datas['actual_visits']}}</td>
                          <td>{{$datas['percent_of_total_target']}}</td>
                          <td>{{$datas['percent_of_ytd']}}</td>

                          <td>{{$datas['total_sales_target']}}</td>
                          <td>{{$datas['ytd_sales_target']}}</td>
                          <td>{{$datas['ytd_products_sold']}}</td>
                          <td>{{$datas['ytd_achieved']}}</td>
                          <td>{{$datas['ytd_conversion']}}</td>


                          <td>{{$datas['today_hh_target']}}</td>
                          <td>{{$datas['today_actual_visits']}}</td>
                          <td>{{$datas['today_percent_of_target']}}</td>
                          <td>{{$datas['today_sales_target']}}</td>
                          <td>{{$datas['today_sales_achieved']}}</td>
                          <td>{{$datas['today_percent_of_sales']}}</td>
                        </tr>
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
            </div>
            <!-- End YTD Table -->
            
        

            
   




            
          </div>
          <!-- /Inner Row Col-md-12 -->
        </div>
        <!-- /Widgets Row End Grid--> 
    </div>
      <!-- / Content Wrapper --> 

@stop