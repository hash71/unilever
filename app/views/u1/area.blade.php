@extends('layouts.default.master')
@section('content') 
  <div class="content-wrapper">
        <!-- 
        Please Dont Delete This portion  <nav class="cbp-hsmenu-wrapper" id="cbp-hsmenu-wrapper"></nav>  
        -->
        <nav class="cbp-hsmenu-wrapper" id="cbp-hsmenu-wrapper"></nav>

        <!--Breadcrumb-->
        <div class="breadcrumb clearfix">
          <ul>
            <li><a href="{{URL::to('/')}}"><i class="fa fa-home"></i></a></li>
            <li><a href="{{URL::to('/')}}">Dashboard</a></li>
            <li class="active">Area Wise Summary</li>
          </ul>
        </div>
        <!--/Breadcrumb--> 
        
          
        <!-- Widget Row Start grid -->
        <div class="row" id="powerwidgets">
                          
          <div class="col-md-12 bootstrap-grid">         
        
            
            <!-- Area Wise Table -->         
            <div class="powerwidget cold-grey" id="areaWise" data-widget-editbutton="false">
              <header>
                <h2>Area Wise Summary</small></h2>
              </header>
              <div class="inner-spacer">
                <div class="cus-table">
                  <div class="table-responsive">
                    <table class="table table-condensed table-bordered table-hover margin-0px">
                      <thead>
                        <tr>
                          <th>Area Wise Summary</th>
                          <th colspan="5">House Holds Status</th>
                          <th colspan="5">Sales Status</th>
                        </tr>
                      </thead>
                      <tbody>
                        <tr>
                          <td><b>Area</b></td>
                          <td><b>HH Target</b></td>
                          <td><b>YTD HH Targer</b></td>
                          <td><b>Actual visits</b></td>
                          <td><b>% of Total Target</b></td>
                          <td><b>% of YTD</b></td>

                          <td><b>Total Sales Target</b></td>
                          <td><b>YTD Sales Target</b></td>
                          <td><b>YTD Products sold</b></td>
                          <td><b>YTD Achieved % (+/-)</b></td>
                          <td><b>YTD Conversion  %</b></td>
                        </tr>
                        
                        @foreach($datas as $data)
                          <tr>
                            <td>{{$data['area_name']}}</td>
                            <td>{{$data['hh_target']}}</td>                            
                            <td>{{$data['ytd_hh_target']}}</td>
                            <td>{{$data['actual_visits']}}%</td>
                            <td>{{$data['percent_of_total_target']}}%</td>
                            <td>{{$data['percent_of_ytd']}}</td>

                            
                            <td>{{$data['total_sales_target']}}</td>
                            <td>{{$data['ytd_sales_target']}}</td>
                            <td>{{$data['ytd_products_sold']}}%</td>
                            <td>{{$data['sales_achieved']}}%</td>
                            <td>{{$data['ytd_conversion']}}%</td>
                          </tr>
                        @endforeach
                      </tbody>
                      <tfoot>
                        <tr>
                          <th>Total</th>                          
                          <th>{{$sum['hh_target']}}</th>
                          <th>{{$sum['ytd_hh_target']}}</th>
                          <th>{{$sum['actual_visits']}}%</th>
                          <th>{{$sum['percent_of_total_target']}}%</th>
                          <th>{{$sum['percent_of_ytd']}}%</th>

                          <th>{{$sum['total_sales_target']}}</th>
                          <th>{{$sum['ytd_sales_target']}}</th>
                          <th>{{$sum['ytd_products_sold']}}</th>
                          <th>{{$sum['sales_achieved']}}%</th>
                          <th>{{$sum['ytd_conversion']}}%</th>
                        </tr>
                      </tfoot>
                    </table>
                  </div>
                </div>
              </div>
            </div>
            <!-- Area Wise Table -->





            
          </div>
          <!-- /Inner Row Col-md-12 -->
        </div>
        <!-- /Widgets Row End Grid--> 
    </div>
      <!-- / Content Wrapper --> 

      @stop