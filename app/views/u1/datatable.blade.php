@extends('layouts.default.master')
@section('content')


<div class="content-wrapper">
    <!-- 
    Please Dont Delete This portion  <nav class="cbp-hsmenu-wrapper" id="cbp-hsmenu-wrapper"></nav>  
-->
<nav class="cbp-hsmenu-wrapper" id="cbp-hsmenu-wrapper"></nav>

<!--Breadcrumb-->
<div class="breadcrumb clearfix">
	<ul>
		<li><a href="{{URL::to('/')}}"><i class="fa fa-home"></i></a></li>
		<li><a href="{{URL::to('/')}}">Dashboard</a></li>
		<li class="active">Full Report</li>
	</ul>
</div>
<!--/Breadcrumb--> 


<!-- Widget Row Start grid -->
<div class="row" id="powerwidgets">

	<div class="col-md-12 bootstrap-grid">
		<!-- D Table -->           
		<div class="powerwidget cold-grey" id="" data-widget-editbutton="false">
			<header>
				<h2>Report Table</small></h2>
			</header>
			<div class="inner-spacer">
				<div class="cus-table">
					<div class="table-responsive">
						<table class="table table-condensed table-bordered table-hover margin-0px" id="reportTable">
							<thead>
								<tr>
									<th>Full Message</th>
									<th>Campaign ID</th>
									<th>Name</th>
									<th>Age</th>
									<th>Gender</th>
									<th>Mobile</th>
									<th>Thana</th>
									<th>District</th>
									<th>Division</th>
									<th>Education</th>
									<th>Occupation</th>
									<th>BP Name</th>
									<th>BP Mobile</th>
									<th>LSA Code</th>
									<th>Coupon</td> 
									<th>Used Product</th>
									<th>Sales Status</th>
									<th>FAL</th>
									<th>PDF</th>
									<th>PWB</th>
									<th>PNS</th>
									<th>PPC</th>
									<th>DBM</th>
									<th>Date</th>
									<th>ERROR</th>                       
								</tr>
									</thead>
									<tbody>
										<tr>
											<td></td>
											<td></td>
											<td></td>
											<td></td>
											<td></td>
											<td></td>
											<td></td>
											<td></td>
											<td></td>
											<td></td>
											<td></td>
											<td></td>
											<td></td>
											<td></td>
											<td></td>
											<td></td>
											<td></td>
											<td></td>
											<td></td>
											<td></td>
											<td></td>
											<td></td>                            
											<td></td>                            
											<td></td>                            
											<td></td>                            
										</tr>
									</tbody>
								</table>
							</div>
						</div>
					</div>
				</div>
				<!-- End YTD Table -->








			</div>
			<!-- /Inner Row Col-md-12 -->
		</div>
		<!-- /Widgets Row End Grid--> 
	</div>
	<!-- / Content Wrapper --> 

	@stop



	@section('u1tableJS')


	<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/plug-ins/f2c75b7247b/integration/bootstrap/3/dataTables.bootstrap.css">
	<script src="http://cdn.datatables.net/1.10.5/js/jquery.dataTables.min.js"></script>
	<script src="http://cdn.datatables.net/plug-ins/f2c75b7247b/integration/bootstrap/3/dataTables.bootstrap.js" ></script>

	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/js/bootstrap.min.js"></script>

	<script>

		var oTable = null;

		$(document).ready(function(){	

			oTable = $('#reportTable').dataTable({
				"scrollX": true,

	// "ajax":'allBlogs',
	"sAjaxSource":'reportdata',
	"bProcessing": true,
	"bServerSide": true,		
	"aoColumns": [
	{ "mDataProp" : "full_message"},
	{ "mDataProp" : "campaign_id"},
	{ "mDataProp" : "customer_name"},
	{ "mDataProp" : "age"},
	{ "mDataProp" : "gender"},
	{ "mDataProp" : "customer_mobile"},
	{ "mDataProp" : "thana"},
	{ "mDataProp" : "district"},
	{ "mDataProp" : "division"},
	{ "mDataProp" : "education_id"},
	{ "mDataProp" : "occupation_id"},
	{ "mDataProp" : "bp_name"},
	{ "mDataProp" : "bp_mobile"},
	{ "mDataProp" : "lsa_code"},
	{ "mDataProp" : "coupon_code"},
	{ "mDataProp" : "currently_used_product"},
	{ "mDataProp" : "sales"},
	{ "mDataProp" : "FAL"},
	{ "mDataProp" : "PDF"},
	{ "mDataProp" : "PWB"},
	{ "mDataProp" : "PNS"},
	{ "mDataProp" : "PPC"},
	{ "mDataProp" : "DBM"},
	{ "mDataProp" : "date"},
	{ "mDataProp" : "error"}
	],           

	// "lengthMenu": [[5,10,15,-1],[5,10,15,"All"]]		
});
		});

	</script>

	@stop