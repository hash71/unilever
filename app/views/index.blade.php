@extends('layouts.default.master')
@section('content')

<div class="content-wrapper">
        <!-- 
        Please Dont Delete This portion  <nav class="cbp-hsmenu-wrapper" id="cbp-hsmenu-wrapper"></nav>  
        -->
        <nav class="cbp-hsmenu-wrapper" id="cbp-hsmenu-wrapper"></nav>

        <!--Breadcrumb-->
        <div class="breadcrumb clearfix">
          <ul>
            <li><a href="{{URL::to('/')}}"><i class="fa fa-home"></i></a></li>
            <li><a href="{{URL::to('/')}}">Dashboard</a></li>
            <li class="active">Data</li>
          </ul>
        </div>
        <!--/Breadcrumb--> 
        
        <div class="row">
          <div class="col-sm-4"></div>
          <div class="col-sm-4">
            <div class="form-group">
              <select class="form-control" name="" id="">
                <option value="">Option One</option>
                <option value="">Option Two</option>
              </select>
            </div>
          </div>
          <div class="col-sm-4">
            <div class="date">
                <div class="form-group">
                  <div class="input-group date">
                    <input type='text' class="form-control" id="datetimepicker1" placeholder="Pick Date"/>
                    <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span>
                    </span>
                  </div>
                </div>
            </div>
          </div>
        </div>
          
        <!-- Widget Row Start grid -->
        <div class="row" id="powerwidgets">

          <div class="col-md-8 bootstrap-grid">
            <!-- New widget -->
            <div class="powerwidget cold-grey" id="morris-stacked-bar" data-widget-editbutton="false">
              <header>
                <h2>Bar Chart</h2>
              </header>
              <div class="inner-spacer">
                <div class="morrischart" id="morris-stacked"></div>
              </div>
            </div>
            <!-- /New widget --> 
            <!-- New widget -->
            <div class="powerwidget cold-grey" id="morrisasline" data-widget-editbutton="false">
              <header>
                <h2>Area Line<small>Lines As Area</small></h2>
              </header>
              <div class="inner-spacer">
                <div class="morrischart" id="morris-area-lines"></div>
              </div>
            </div>
            <!-- /New widget -->
          </div>
          <!-- /Inner Row Col-md-8 -->
                          
          <div class="col-md-4 bootstrap-grid"> 
            <!-- New widget -->
            <div class="powerwidget cold-grey" id="flotchart-widget-1" data-widget-editbutton="false">
              <header>
                <h2>Pie Chart<small>FlotChart</small></h2>
              </header>
              <div class="inner-spacer">
                <div class="flotchart-container">
                  <div id="placeholder6a" class="flotchart-placeholder"></div>
                </div>
              </div>
            </div>
            <!-- End Widget -->
          </div>
          <!-- /Inner Row Col-md-4 -->

          <div class="clearfix"></div>
                         
          <div class="col-md-4 bootstrap-grid">

            <!-- New widget -->
            <div class="powerwidget cold-grey" id="flotchart-widget-2" data-widget-editbutton="false">
              <header>
                <h2>Pie Chart<small>FlotChart</small></h2>
              </header>
              <div class="inner-spacer">
                <div class="flotchart-container">
                  <div id="placeholder6b" class="flotchart-placeholder"></div>
                </div>
              </div>
            </div>
            <!-- End Widget -->
          </div>
          <!-- /Inner Row Col-md-4 -->             
                         
          <div class="col-md-4 bootstrap-grid"> 
            <!-- New widget -->
            <div class="powerwidget cold-grey" id="flotchart-widget-3" data-widget-editbutton="false">
              <header>
                <h2>Pie Chart<small>FlotChart</small></h2>
              </header>
              <div class="inner-spacer">
                <div class="flotchart-container">
                  <div id="placeholder6c" class="flotchart-placeholder"></div>
                </div>
              </div>
            </div>
            <!-- End Widget -->
          </div>
          <!-- /Inner Row Col-md-4 -->             
                         
          <div class="col-md-4 bootstrap-grid"> 
            <!-- New widget -->
            <div class="powerwidget cold-grey" id="flotchart-widget-4" data-widget-editbutton="false">
              <header>
                <h2>Pie Chart<small>FlotChart</small></h2>
              </header>
              <div class="inner-spacer">
                <div class="flotchart-container">
                  <div id="placeholder6d" class="flotchart-placeholder"></div>
                </div>
              </div>
            </div>
            <!-- End Widget -->
          </div>
          <!-- /Inner Row Col-md-4 --> 
        </div>
        <!-- /Widgets Row End Grid--> 
    </div>
      <!-- / Content Wrapper --> 

@stop