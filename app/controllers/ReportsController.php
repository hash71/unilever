<?php

class ReportsController extends \BaseController {

	public function showAllBlogs(){

		// return "hello";
		
		return Datatable::collection(Area::all())
			->searchColumns('_id','thana','district')
			->orderColumns('_id')
			->addColumn('ID',function($model){
				return $model->id;
			})
			->addColumn('THANA',function($model){
				return $model->thana;
			})
			->addColumn('DISTRICT',function($model){
				return $model->district;
			})
			->make();
	}

	public function ajax(){
		// return "balda";
		$cols = [
				'full_message',
				'campaign_id',
				'customer_name',
				'age',
				'gender',
				'customer_mobile',
				'thana',
				'district',
				'division',
				'education_id',
				'occupation_id',
				'bp_name',
				'bp_mobile',
				'lsa_code',
				'coupon_code',
				'currently_used_product',
				'sales',
				'FAL',
				'PDF',
				'PWB',
				'PNS',
				'PPC',
				'DBM',
				'date',
				'error'
				];		
		
		return DTmongo::ajax('u1_messages',$cols);
	}
}
