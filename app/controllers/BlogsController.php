<?php

class BlogsController extends \BaseController {

	public function showAllBlogs(){

		// return "hello";
		
		return Datatable::collection(Area::all())
			->searchColumns('_id','thana','district')
			->orderColumns('_id')
			->addColumn('ID',function($model){
				return $model->id;
			})
			->addColumn('THANA',function($model){
				return $model->thana;
			})
			->addColumn('DISTRICT',function($model){
				return $model->district;
			})
			->make();
	}

	public function ajax(){
		$cols = ['_id','thana','district'];
		return DTmongo::test();
		return DTmongo::ajax($cols);
	}
}
