<?php

class MonthWiseController extends \BaseController {

	public function index($campaign){		

		$datas = array();
		$sums = array();
		
		$campaign_total_days = $this->campaign_timeframe($campaign);

		$day_passed_till_now =  $this->days_passed($campaign);

		foreach (Tmonth::where('campaign',$campaign)->get() as $month) {
			
			$row['month_name'] = $month->month;
			
			$row['hh_target'] = $month->target;
			
			$row['ytd_hh_target'] = $this->ytd_target($campaign,$month->month_number);

			$row['actual_visits'] = $this->visits($campaign,$month->month_number);


			$row['percent_of_total_target'] = $row['hh_target']?round(($row['actual_visits']/$row['hh_target'])*100):0;


			$row['percent_of_ytd'] = round(($row['actual_visits']/$row['ytd_hh_target'])*100);

			$row['total_sales_target'] = TsalesMonth::where('campaign',$campaign)
										 ->where('month_number',$month->month_number)
										 ->pluck('target');			

			$row['ytd_sales_target'] = $this->ytd_sales_target($campaign,$month->month_number);

			$row['ytd_products_sold'] = $this->ytd_sold($campaign,$month->month_number);

			$row['ytd_sales_achieved'] = round(($row['ytd_products_sold']/$row['ytd_sales_target'])*100);

			$row['ytd_conversion'] = $row['actual_visits']?round(($row['ytd_products_sold']/$row['actual_visits'])*100):0;

			array_push($datas, $row);
		}
		
		$sum = $this->sum_row($datas);

		return View::make('u1.month',compact('datas','sum'));
		
		

		return dd($datas);

	}

	public function sum_row($datas){
		
		$sum['hh_target'] = 0;
		$sum['ytd_hh_target'] = 0;
		$sum['actual_visits'] = 0;
		$sum['percent_of_total_target'] = 0;
		$sum['percent_of_ytd'] = 0;
		$sum['total_sales_target'] = 0;
		$sum['ytd_sales_target'] = 0;
		$sum['ytd_products_sold'] = 0;
		$sum['ytd_sales_achieved'] = 0;
		$sum['ytd_conversion'] = 0;
	

		foreach ($datas as $data) {
			
			$sum['hh_target'] += $data['hh_target'];
			$sum['ytd_hh_target'] += $data['ytd_hh_target'];
			$sum['actual_visits'] += $data['actual_visits'];
			$sum['percent_of_total_target'] += $data['percent_of_total_target'];
			$sum['percent_of_ytd'] += $data['percent_of_ytd'];
			$sum['total_sales_target'] += $data['total_sales_target'];
			$sum['ytd_sales_target'] += $data['ytd_sales_target'];
			$sum['ytd_products_sold'] += $data['ytd_products_sold'];
			$sum['ytd_sales_achieved'] += $data['ytd_sales_achieved'];
			$sum['ytd_conversion'] += $data['ytd_conversion'];

		}
		return $sum;
	}

	public function campaign_timeframe($campaign){

		$start = Tmonth::where('campaign',$campaign)
				->orderBy('month_number')
				->pluck('month_number');

		$end = Tmonth::where('campaign',$campaign)
				->orderBy('month_number','desc')
				->pluck('month_number');

		$start = date_create(date("Y-$start-01"));

		$end = date_create(date("Y-$end-t"));

		$total_days = date_diff($end,$start);

		return $total_days = $total_days->days+1;
	}

	public function days_passed($campaign){

		$start = Tmonth::where('campaign',$campaign)
				->orderBy('month_number')
				->pluck('month_number');

		

		$start = date_create(date("Y-$start-01"));

		$end = date_create(date("Y-m-d"));

		$total_days = date_diff($end,$start);

		return $total_days = $total_days->days + 1;

	}

	public function ytd_sold($campaign,$month){
		

		$products = Product::where('campaign',$campaign)
							->where('type','sold')
							->lists('product_code');

		$d = 0;

		$month++;

		$time = strtotime(date("Y-$month-01"));//next month first date
		
		foreach ($products as $product) {
			
			$d += Message::where('campaign_id',$campaign)				
					->where('time','<',$time)							
					->count($product);


		}

		return $d;


	}

	public function visits($campaign,$month){
		
		$next_month = $month+1;

		$start = strtotime(date("Y-$month-01"));
		$end   = strtotime(date("Y-$next_month-01"));

		return $visits = Message::where('campaign_id',$campaign)
				 ->where('time','>=',$start)
				 ->where('time','<',$end)
				 ->count();

	}

	public function ytd_target($campaign,$month){

		if(date('m') == $month){//current month

			$target = Tmonth::where('month_number','<',$month)->sum('target');



			$target += (Tmonth::where('month_number',$month)->pluck('target')/date('t'))*date('d');	

			return round($target);		

		}else{

			return Tmonth::where('month_number','<=',$month)->sum('target');


		}

	}

	public function ytd_sales_target($campaign,$month){

		if( date('m') == $month ){//current month

			$target = TSalesMonth::where('month_number','<',$month)->sum('target');


			$target += (TSalesMonth::where('month_number',$month)->pluck('target')/date('t'))*date('d');	

			return round($target);		

		}else{

			return TSalesMonth::where('month_number','<=',$month)->sum('target');


		}

	}





}