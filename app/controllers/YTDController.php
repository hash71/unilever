<?php

class YTDController extends \BaseController {

	public function index($campaign){

		$datas = array();

		$datas['camp'] = $campaign;

		$datas['hh_target'] = Tmonth::where('campaign',$campaign)->sum('target');

		$datas['ytd_hh_target'] = $this->target_till_now('Tmonth',$campaign);		

		$datas['actual_visits'] = Message::where('campaign_id',$campaign)->count();
				
		$datas['percent_of_total_target']= round(($datas['actual_visits']/$datas['hh_target'])*100,2);

		$datas['percent_of_ytd'] = round(($datas['actual_visits']/$datas['ytd_hh_target'])*100,2);

		$datas['total_sales_target'] = TSalesMonth::where('campaign',$campaign)->sum('target');

		$datas['ytd_sales_target'] = $this->target_till_now('TSalesMonth',$campaign);		

		$products = Product::where('campaign',$campaign)
							->where('type','sold')
							->lists('product_code');

		$datas['ytd_products_sold'] = 0;
		
		foreach ($products as $product) {
			
			$datas['ytd_products_sold'] += Message::where('campaign_id',$campaign)				
											->where('time','<=',time())
											->count($product);


		}


		$datas['ytd_achieved'] = round(($datas['ytd_products_sold']/$datas['ytd_sales_target'])*100);

		$datas['ytd_conversion'] = round(($datas['ytd_products_sold']/$datas['actual_visits'])*100);

		$datas['today_hh_target'] = round(Tmonth::where('month_number',date('m'))->where('campaign',$campaign)->pluck('target')/date('t'));	


		$datas['today_actual_visits'] = Message::where('campaign_id',$campaign)
										->where('time','>=',strtotime(date('Y-m-d')))
										->where('time','<=',time())
										->count();

		$datas['today_percent_of_target'] = round(($datas['today_actual_visits']/$datas['today_hh_target'])*100);

		$datas['today_sales_target'] = round(TSalesMonth::where('month_number',date('m'))->where('campaign',$campaign)->pluck('target')/date('t'));

		$datas['today_sales_achieved'] = Message::where('campaign_id',$campaign)
										->where('time','>=',strtotime(date('Y-m-d')))
										->where('time','<=',time())
										->where('sales','Y')
										->count();


		$datas['today_percent_of_sales'] = round(($datas['today_sales_achieved']/$datas['today_actual_visits'])*100);

		// return dd($datas);

		return View::make('u1.ytd',compact('datas'));
		
	}	

	public function target_till_now($model,$campaign){

		// return date("t");

		$target_current_month = $model::where('month_number',date('m'))->where('campaign',$campaign)->pluck('target');

		$target_per_day_of_current_month = ($target_current_month / date("t"));

		$start = date_create((date("Y-m-01")));

		$today = date_create((date("Y-m-d")));		

		$date_passed_this_month = date_diff($today,$start)->days + 1;

		$target_till_now_of_current_month = $target_per_day_of_current_month * $date_passed_this_month;

		$target_including_this_month = $model::where('month_number','<=',date('m'))->where('campaign',$campaign)->sum('target');

		$target_excluding_this_month = $target_including_this_month - $target_current_month;


		$target_now  = $target_till_now_of_current_month + $target_excluding_this_month;

		$target_now = round($target_now);
		
		return round($target_now);
		
	}


}