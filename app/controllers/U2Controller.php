<?php

 	/*
http://localhost/unilever/public/api_u2/c1Wt08MRDhdTce7vFNF8BYNNiImjgFeDjsqbkkK6KbWYkji7rFV+UjIDaVbgwYKt3E0pVeMKVYAz5N2QWrOBFQ==/0196229909/u2 hasan,30,f,01710340450,1001,5,1,88865652,2,y,1:2

 	*/



class U2Controller extends \BaseController {

	
	public function parse($token,$bp_mobile,$message){
			

		$token_valid = $this->validate_token($token);
		
		$bp_mobile_valid = $this->validate_mobile($bp_mobile);

		$msg_len = strlen($message);

		if( !$token_valid ) return;

		$msg = new Message;

		$msg->full_message = $message;

		$msg->bp_mobile = $bp_mobile;

		$msg->bp_name = BP::where('mobile',$bp_mobile."")
						->pluck('name');						
		$msg->lsa_code = BP::where('mobile',$bp_mobile."")
						->pluck('lsa_code');

		$msg->date = date("Y-m-d h:i:s");

		$errors = null;

		if(substr($message, 0, 3) != 'u2 '){//there must be a space preceding proper campaign id

			$errors = 'Campaign id not valid';				
			
			$msg->error = $errors;		

			$this->store($msg);

			return;				

		}

		$pos = strpos($message, ' ');
						
		$message[$pos] = ',';//replacing the space after u1 with ,

		$message = explode(",",$message);//$message is an array of tokens


		if(sizeof($message) < 11){// 11th number field is yes/no

			$errors = "Insufficient number of fields";			

			$msg->error = $errors;

			$this->store($msg);

			return;

		}

		if($msg_len > 160){//if message length exceeding 

			$errors = "Message Length Exceeded";			

			$msg->error = $errors;

			$this->store($msg);

			return;

		}

		

		if( !strlen($message[1]) ){//customer name

			$errors = "Customer name field blank";	

			$msg->error = $errors;

			$this->store($msg);

			return;

		}else{
			
				if( strlen($message[1]) > 15 ){

					$errors = "Customer name more than 15 characters";

					$msg->error = $errors;

					$this->store($msg);

					return;

				}

		}


		if(!strlen($message[2])){//age

			$errors = "Customer age field blank";

			$msg->error = $errors;

			$this->store($msg);

			return;

		}else{

			if( !ctype_digit($message[2]) ){

				$errors = "Customer age not numeric";

				$msg->error = $errors;

				$this->store($msg);

				return;

			}

			if( $message[2] < 10 || $message[2] > 100  ){

				$errors = "Customer age not in limit";

				$msg->error = $errors;

				$this->store($msg);

				return;

			}
		
		}




		if(!strlen($message[3])){//gender

			$errors = "Customer gender field is blank";

			$msg->error = $errors;

			$this->store($msg);

			return;

		}else{

			if($message[3] !== 'm' && $message[3] !=='f' && $message[3] !== 'M' && $message[3] !== 'F'){

				$errors = "Customer gender type undefined";

				$msg->error = $errors;

				$this->store($msg);

				return;
			}
			
		}








		if( !strlen($message[4]) ){//customer mobile



			$errors = "Customer mobile field blank";

			$msg->error = $errors;

			$this->store($msg);



			return;

		}else if( !preg_match("/^01[0-9]{9}$/", $message[4])){//changed here
			
			$errors = "Customer mobile invalid format";

			$msg->error = $errors;

			$this->store($msg);

			return;

		}else{

			if( strlen($message[4]) != 11 ){

				$errors = "Customer mobile field not 11 digit";

				$msg->error = $errors;

				$this->store($msg);

				return;
			}

		}

		



		if( strlen($message[5])!=4 ){//area code


			$errors = "Thana code invalid format";

			$msg->error = $errors;

			$this->store($msg);

			return;

		}else{



			if( !in_array($message[5], Area::lists('area_code'))){

				$errors = "Thana code not found in database";

				$msg->error = $errors;

				$this->store($msg);

				return;

			}
		
		}





		if(!strlen($message[6])){//education

			$errors = "Education field blank";

			$msg->error = $errors;

			$this->store($msg);

			return;

		}else{

			if($message[6] < 1 || $message[6] > 5){

				$errors = "Education value not in range";

				$msg->error = $errors;

				$this->store($msg);

				return;

			}
		}



		if(!strlen($message[7])){//occupation

			$errors = "Occupation field blank";

			$msg->error = $errors;

			$this->store($msg);

			return;

		}else{

			if($message[7] < 1 || $message[7] > 4 ){

				$errors = "Occupation value not in range";

				$msg->error = $errors;

				$this->store($msg);

				return;

			}

		}


		if(!strlen($message[8])){//coupon

			$errors = "Coupon Code Blank";		

			$msg->error = $errors;

			$this->store($msg);

			return;	

		}else{

			if( !in_array($message[8], Coupon::lists('coupon_code')) ){

				$errors = "Coupon Code not listed in Database";	

				$msg->error = $errors;

				$this->store($msg);

				return;		

			}
		}





		if(!strlen($message[9])){//currently using product

			$errors = "Currently Using Products Field blank";

			$msg->error = $errors;

			$this->store($msg);

			return;		

		}else{

			if($message[9] < 0 || $message[9] > 4){

				$errors = "Currently Using Product Not Listed";		

				$msg->error = $errors;

				$this->store($msg);

				return;		
	
			}
		
		}






		if(!strlen($message[10])){//sales yes or no

			$errors = "Sales Field Blank";

			$msg->error = $errors;

			$this->store($msg);

			return;	

		}else{

			if($message[10] !='y' && $message[10] !='n' && $message[10] !='Y' && $message[10] !='N'){

				$errors = "Sales format invalid";

				$msg->error = $errors;

				$this->store($msg);

				return;	
			}
		}


		//if no error then save fields
		
		$msg->campaign_id = $message[0];			
		$msg->customer_name = $message[1];
		$msg->age = $message[2];
		$msg->gender = ucfirst($message[3]);
		$msg->customer_mobile = $message[4];
		$msg->area_code = $message[5];
		$msg->education_id = $message[6];
		$msg->occupation_id = $message[7];
		$msg->coupon_code = $message[8];
		$msg->currently_used_product_table_id = $message[9];
		$msg->sales = ucfirst($message[10]);

		

		$msg->thana = Area::where('area_code',$message[5]."")
					  ->pluck('thana');
		$msg->district = Area::where('area_code',$message[5]."")
					  ->pluck('district');
		$msg->division = Area::where('area_code',$message[5]."")
					  ->pluck('division');
		

		$msg->save();
		

	}

	
	public function validate_token($token){

		$key = '@u@1423uH@u05H@k1uGw1TH1551';
		
		$string = 'r;w{h3R$M|u/&RPZOPaIZ?-_=|:&zy_i*|gfYr2.eB72xPi01b30V&%d~O5{%!mQ';

		$encrypted = $token;

		$decrypted = rtrim(mcrypt_decrypt(MCRYPT_RIJNDAEL_256, md5($key), base64_decode($encrypted), MCRYPT_MODE_CBC, md5(md5($key))), "\0");

		return ($decrypted === $string)?true:false;
	
	}

	public function validate_mobile($mobile){

		$bp = BP::where('mobile','bp_mobile')->get();

		return $bp?true:false;

	}

	public function store($msg){

			$message->save();
			
			U2Message::create([

				'full_message'=>$msg->full_message,
				'campaign_id'=>$msg->campaign_id,
				'customer_name'=>$msg->customer_name?$msg->customer_name:"",
				'age'=>$msg->age?$msg->age:0,
				'gender'=>$msg->gender?$msg->gender:"",
				'customer_mobile'=>$msg->customer_mobile?$msg->customer_mobile:"",
				'area_code'=>$msg->area_code?$msg->area_code:0,
				'education_id'=>$msg->education_id?$msg->education_id:0,
				'occupation_id'=>$msg->occupation_id?$msg->occupation_id:0,
				'coupon_code'=>$msg->coupon_code?$msg->coupon_code:0,
				'currently_used_product'=>$msg->currently_used_product?$msg->currently_used_product:0,
				'sales'=>$msg->sales?$msg->sales:0,				
				'bp_mobile'=>$msg->bp_mobile?$msg->bp_mobile:'',
				'time'=>$msg->time?$msg->time:'',
				

				'thana' => $msg->thana?$msg->thana:'',
				'district' => $msg->district?$msg->district:'',
				'division' => $msg->division?$msg->division:'',

				'bp_name' => $msg->bp_name?$msg->bp_name:'',						
				'lsa_code' => $msg->lsa_code?$msg->lsa_code:'',

				'date' => $msg->date?$msg->date:'',

				'error'=>$msg->error?$msg->error:''

			]);

	}





}