<?php

class AreaWiseController extends \BaseController {

	public function index($campaign){		

		$datas = array();
		
		$campaign_total_days = $this->campaign_timeframe($campaign);

		$day_passed_till_now =  $this->days_passed($campaign);

		foreach (Tarea::where('campaign',$campaign)->get() as $area) {
			
			$row['area_name'] = Area::where('area_code',$area->area."")->pluck('thana');
			
			$row['hh_target'] = $area->target;

			$row['percent'] = round($row['hh_target']/(Tmonth::where('campaign',$campaign)->sum('target'))*100);

			$target_per_day = $row['hh_target']/$campaign_total_days;

			$row['ytd_hh_target'] = round($target_per_day * $day_passed_till_now);

			$row['actual_visits'] = Message::where('campaign_id',$campaign)
									->where('area_code',$area->area)
									->count();


			$row['percent_of_total_target'] = round(($row['actual_visits']/$row['hh_target'])*100);

			$row['percent_of_ytd'] = round(($row['actual_visits']/$row['ytd_hh_target'])*100);

			$row['total_sales_target'] = TsalesArea::where('campaign',$campaign)
										 ->where('area',$area->area)
										 ->pluck('target');

			$sales_target_per_day = $row['total_sales_target']/$campaign_total_days;

			$row['ytd_sales_target'] = round($sales_target_per_day * $day_passed_till_now);

			$row['ytd_products_sold'] = $this->ytd_sold($campaign,$area->area);

			$row['sales_achieved'] = round(($row['ytd_products_sold']/$row['ytd_sales_target'])*100);

			$row['ytd_conversion'] = round(($row['ytd_products_sold']/$row['actual_visits'])*100);

			array_push($datas, $row);
		}

		$sum = $this->sum_row($datas);

		return View::make('u1.area',compact('datas','sum'));


		return dd($datas);

	}

	public function sum_row($datas){

		$sum['hh_target']=0;
		$sum['percent']=0;
		$sum['ytd_hh_target']=0;
		$sum['actual_visits']=0;
		$sum['percent_of_total_target']=0;
		$sum['percent_of_ytd']=0;
		$sum['total_sales_target']=0;
		$sum['ytd_sales_target']=0;
		$sum['ytd_products_sold']=0;
		$sum['sales_achieved']=0;
		$sum['ytd_conversion']=0;
		
		foreach ($datas as $data) {
			
			$sum['hh_target']+=$data['hh_target'];
			$sum['percent']+=$data['percent'];
			$sum['ytd_hh_target']+=$data['ytd_hh_target'];
			$sum['actual_visits']+=$data['actual_visits'];
			$sum['percent_of_total_target']+=$data['percent_of_total_target'];
			$sum['percent_of_ytd']+=$data['percent_of_ytd'];
			$sum['total_sales_target']+=$data['total_sales_target'];
			$sum['ytd_sales_target']+=$data['ytd_sales_target'];
			$sum['ytd_products_sold']+=$data['ytd_products_sold'];
			$sum['sales_achieved']+=$data['sales_achieved'];
			$sum['ytd_conversion']+=$data['ytd_conversion'];
			

		}

		return $sum;



	}

	public function campaign_timeframe($campaign){

		$start = Tmonth::where('campaign',$campaign)
				->orderBy('month_number')
				->pluck('month_number');

		$end = Tmonth::where('campaign',$campaign)
				->orderBy('month_number','desc')
				->pluck('month_number');

		$start = date_create(date("Y-$start-01"));

		$end = date_create(date("Y-$end-t"));

		$total_days = date_diff($end,$start);

		return $total_days = $total_days->days+1;
	}

	public function days_passed($campaign){

		$start = Tmonth::where('campaign',$campaign)
				->orderBy('month_number')
				->pluck('month_number');

		

		$start = date_create(date("Y-$start-01"));

		$end = date_create(date("Y-m-d"));

		$total_days = date_diff($end,$start);

		return $total_days = $total_days->days + 1;

	}

	public function ytd_sold($campaign,$area_code){
		

		$products = Product::where('campaign',$campaign)
							->where('type','sold')
							->lists('product_code');

		$d = 0;
		
		foreach ($products as $product) {
			
			$d += Message::where('campaign_id',$campaign)				
											->where('time','<=',time())
											->where('area_code',$area_code)
											->count($product);


		}

		return $d;


	}





}