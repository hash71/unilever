<?php

use Faker\Factory as Faker;

Route::get('test', function(){

	$x = Message::orderBy('_id','desc')->first();

	return dd($x);


});

Route::get('allBlogs', 'BlogsController@showAllBlogs');

Route::get('allBlogs1', 'BlogsController@ajax');

Route::get('reportdata', 'ReportsController@ajax');


Route::get('u1/ytd', function(){	
	
	return View::make('u1.ytd');
});

Route::get('u1/month', function(){	
	
	return View::make('u1.month');
});

Route::get('u1/area', function(){	
	
	return View::make('u1.area');
});


Route::get('table',function(){
	return View::make('table');
});

Route::get('table1',function(){
	// return DT::test();
	return View::make('table1');
});

Route::get('fullreport', function(){

	return View::make('fullReport');
});

Route::get('api_u1/{token}/{bp_mobile}/{message}','U1Controller@parse');


Route::get('api_u2/{token}/{bp_mobile}/{message}','U2Controller@parse');


Route::get('target_month',function(){

	DB::collection('target_month')->truncate();

	DB::collection('target_month')->insert([

		[

			'campaign'=>'u1',
			'month'=>'February',
			'month_number'=>'02',
			'target'=>31800

		],
		[

			'campaign'=>'u1',
			'month'=>'March',
			'month_number'=>'03',
			'target'=>84000

		],
		[

			'campaign'=>'u1',
			'month'=>'April',
			'month_number'=>'04',
			'target'=>84000

		],
		[

			'campaign'=>'u1',
			'month'=>'May',
			'month_number'=>'05',
			'target'=>84000

		],
		[

			'campaign'=>'u1',
			'month'=>'June',
			'month_number'=>'06',
			'target'=>50400

		],
		[

			'campaign'=>'u1',
			'month'=>'July',
			'month_number'=>'07',
			'target'=>0

		],
		[

			'campaign'=>'u1',
			'month'=>'August',
			'month_number'=>'08',
			'target'=>66600

		],
		[

			'campaign'=>'u1',
			'month'=>'September',
			'month_number'=>'09',
			'target'=>59100

		],
		[

			'campaign'=>'u1',
			'month'=>'October',
			'month_number'=>'10',
			'target'=>40500

		]
	]);
});


Route::get('target_area',function(){

	DB::collection('target_area')->insert([

		[
			'campaign'=>'u1',
			'area'=>1027,
			'target'=>6

		],
		[
			'campaign'=>'u1',
			'area'=>1032,
			'target'=>8

		],
		[
			'campaign'=>'u1',
			'area'=>1017,
			'target'=>5

		],
		[
			'campaign'=>'u1',
			'area'=>1019,
			'target'=>5

		],
		[
			'campaign'=>'u1',
			'area'=>1058,
			'target'=>25

		],
		[
			'campaign'=>'u1',
			'area'=>1127,
			'target'=>17

		],
		[
			'campaign'=>'u1',
			'area'=>1022,
			'target'=>9

		],
		[
			'campaign'=>'u1',
			'area'=>1037,
			'target'=>20

		],
		[
			'campaign'=>'u1',
			'area'=>1018,
			'target'=>5

		]


	]);
});

Route::get('insert_products',function(){
	
	Product::truncate();

	Product::insert([
		[	
			'campaign'=>'u1',
			'product_name'=>'UBL FW',
			'product_id'=>1,
			'type'=>'using'
		],
		[	
			'campaign'=>'u1',
			'product_name'=>'Comp. FW',
			'product_id'=>2,
			'type'=>'using'
		],
		[	
			'campaign'=>'u1',
			'product_name'=>'Soap User',
			'product_id'=>3,
			'type'=>'using'
		],
		[	
			'campaign'=>'u1',
			'product_name'=>'Proxy User',
			'product_id'=>4,
			'type'=>'using'
		],
		[	
			'campaign'=>'u1',
			'product_name'=>'None',
			'product_id'=>0,
			'type'=>'using'
		],
		[	
			'campaign'=>'u1',
			'product_name'=>'Fair & Lovely Facewash 50g',
			'product_id'=>1,
			'product_code'=>'FAL',
			'type'=>'sold'
		],
		[	
			'campaign'=>'u1',
			'product_name'=>'Pond’s Daily Facewash 60g ',
			'product_id'=>2,
			'product_code'=>'PDF',
			'type'=>'sold'
		],
		[	
			'campaign'=>'u1',
			'product_name'=>'Pond’s White Beauty Facewash 50g ',
			'product_id'=>3,
			'product_code'=>'PWB',
			'type'=>'sold'
		],
		[	
			'campaign'=>'u1',
			'product_name'=>'Pond’s Nourishing Scrub Facewash 110g ',
			'product_id'=>4,
			'product_code'=>'PNS',
			'type'=>'sold'
		],
		[	
			'campaign'=>'u1',
			'product_name'=>'Pond’s Pimple Clear White Facewash 50g ',
			'product_id'=>5,
			'product_code'=>'PPC',
			'type'=>'sold'
		],
		[	
			'campaign'=>'u1',
			'product_name'=>'Dove Beauty Moisture Facewash 50g ',
			'product_id'=>6,
			'product_code'=>'DBM',
			'type'=>'sold'
		]
	]);

});

Route::get('target_sales_month',function(){

	DB::collection('target_sales_month')->truncate();

	DB::collection('target_sales_month')->insert([

		[

			'campaign'=>'u1',
			'month'=>'February',
			'month_number'=>'02',
			'target'=>21800

		],
		[

			'campaign'=>'u1',
			'month'=>'March',
			'month_number'=>'03',
			'target'=>64000

		],
		[

			'campaign'=>'u1',
			'month'=>'April',
			'month_number'=>'04',
			'target'=>74000

		],
		[

			'campaign'=>'u1',
			'month'=>'May',
			'month_number'=>'05',
			'target'=>74000

		],
		[

			'campaign'=>'u1',
			'month'=>'June',
			'month_number'=>'06',
			'target'=>60400

		],
		[

			'campaign'=>'u1',
			'month'=>'July',
			'month_number'=>'07',
			'target'=>0

		],
		[

			'campaign'=>'u1',
			'month'=>'August',
			'month_number'=>'08',
			'target'=>56600

		],
		[

			'campaign'=>'u1',
			'month'=>'September',
			'month_number'=>'09',
			'target'=>49100

		],
		[

			'campaign'=>'u1',
			'month'=>'October',
			'month_number'=>'10',
			'target'=>30500

		]
	]);
});

Route::get('target_sales_area',function(){

	DB::collection('target_sales_area')->insert([

		[
			'campaign'=>'u1',
			'area'=>1027,
			'target'=>6000

		],
		[
			'campaign'=>'u1',
			'area'=>1032,
			'target'=>7980

		],
		[
			'campaign'=>'u1',
			'area'=>1017,
			'target'=>5040

		],
		[
			'campaign'=>'u1',
			'area'=>1019,
			'target'=>5040

		],
		[
			'campaign'=>'u1',
			'area'=>1058,
			'target'=>25020

		],
		[
			'campaign'=>'u1',
			'area'=>1127,
			'target'=>17040

		],
		[
			'campaign'=>'u1',
			'area'=>1022,
			'target'=>8940

		],
		[
			'campaign'=>'u1',
			'area'=>1037,
			'target'=>19980

		],
		[
			'campaign'=>'u1',
			'area'=>1018,
			'target'=>5040

		]


	]);
});



Route::get('/', function(){

	return View::make('index');

});

Route::get('areawise/{campaign}', 'AreaWiseController@index');

Route::get('monthwise/{campaign}', 'MonthWiseController@index');

Route::get('ytd/{campaign}','YTDController@index');

Route::get('u1table', function(){

	return View::make('u1.datatable');

});

