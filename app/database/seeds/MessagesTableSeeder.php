<?php

// Composer: "fzaninotto/faker": "v1.3.0"
use Faker\Factory as Faker;

class MessagesTableSeeder extends Seeder {

	public function run()
	{
		U1Message::truncate();

		$faker = Faker::create();
		
		//regular
		foreach(range(1, 1000) as $index)
		{
			
			
			$cnumber = '017'.$faker->randomNumber($nbDigits = 8);

			if(strlen($cnumber)<11){
				while(1){
					if(strlen($cnumber) == 11)break;
					$cnumber .= "1";
				}
			}			

			$d = $faker->dateTimeBetween($startDate = '-23 days', $endDate = '+6 months');

			$d  = $d->format('Y-m-d H:i:s');

			$d = strtotime($d);

			$area_code = $faker->randomElement(Tarea::lists('area'));
			
			$bp_mobile = $faker->randomElement(BP::lists('mobile'));

			$msg = Message::create([

				'full_message'=>'This is full message',
				'campaign_id'=>'u1',
				'customer_name'=>$faker->name,
				'age'=>$faker->numberBetween($min = 15, $max = 70),
				'gender'=>$faker->randomElement(['M','F']),
				'customer_mobile'=>$cnumber,
				'area_code'=>$area_code,
				'education_id'=>$faker->randomElement([1,2,3,4,5]),
				'occupation_id'=>$faker->numberBetween($min = 1, $max = 4),
				'coupon_code'=>$faker->randomElement(Coupon::lists('coupon_code')),
				'currently_used_product'=>$faker->randomElement([1,2,3,4,5]),
				'sales'=>'Y',				
				'bp_mobile'=>$bp_mobile,
				'time'=>$d,
				'FAL'=>$faker->randomDigit,
				'PDF'=>$faker->randomDigit,
				'PWB'=>$faker->randomDigit,
				'PNS'=>$faker->randomDigit,
				'PPC'=>$faker->randomDigit,
				'DBM'=>$faker->randomDigit,

				'thana' => Area::where('area_code',$area_code."")
					  ->pluck('thana'),
				'district' => Area::where('area_code',$area_code."")
					  ->pluck('district'),
				'division' => Area::where('area_code',$area_code."")
					  ->pluck('division'),

				'bp_name' => BP::where('mobile',$bp_mobile."")
						->pluck('name'),						
				'lsa_code' => BP::where('mobile',$bp_mobile."")
						->pluck('lsa_code'),

				'date' => date("Y-m-d h:i:s")

			]);

			$this->u1($msg);


		}


		//no sale
		foreach(range(1, 300) as $index)
		{
			
			
			$cnumber = '017'.$faker->randomNumber($nbDigits = 8);

			if(strlen($cnumber)<11){
				while(1){
					if(strlen($cnumber) == 11)break;
					$cnumber .= "1";
				}
			}	

			$d = $faker->dateTimeBetween($startDate = '-23 days', $endDate = '+6 months');

			$d  = $d->format('Y-m-d H:i:s');

			$d = strtotime($d);

			$area_code = $faker->randomElement(Tarea::lists('area'));
			
			$bp_mobile = $faker->randomElement(BP::lists('mobile'));

			$msg = Message::create([

				'full_message'=>'This is full message',
				'campaign_id'=>'u1',
				'customer_name'=>$faker->name,
				'age'=>$faker->numberBetween($min = 15, $max = 70),
				'gender'=>$faker->randomElement(['M','F']),
				'customer_mobile'=>$cnumber,
				'area_code'=>$area_code,
				'education_id'=>$faker->randomElement([1,2,3,4,5]),
				'occupation_id'=>$faker->numberBetween($min = 1, $max = 4),
				'coupon_code'=>$faker->randomElement(Coupon::lists('coupon_code')),
				'currently_used_product'=>$faker->randomElement([1,2,3,4,5]),
				'sales'=>'N',				
				'bp_mobile'=>$bp_mobile,
				'time'=>$d,			

				'thana' => Area::where('area_code',$area_code."")
					  ->pluck('thana'),
				'district' => Area::where('area_code',$area_code."")
					  ->pluck('district'),
				'division' => Area::where('area_code',$area_code."")
					  ->pluck('division'),

				'bp_name' => BP::where('mobile',$bp_mobile."")
						->pluck('name'),						
				'lsa_code' => BP::where('mobile',$bp_mobile."")
						->pluck('lsa_code'),

				'date' => date("Y-m-d h:i:s")

			]);

			$this->u1($msg);
		}



		//error
		foreach(range(1, 200) as $index)
		{

			$d = $faker->dateTimeBetween($startDate = '-23 days', $endDate = '+6 months');

			$d  = $d->format('Y-m-d H:i:s');

			$d = strtotime($d);

			$bp_mobile = $faker->randomElement(BP::lists('mobile'));

			$msg = Message::create([

				'full_message'=>'This is full message',
				'campaign_id'=>'u1',				
				'bp_mobile'=>$bp_mobile,
				'time'=>$d,
				'error'=>'This is an error',
				'date' => date("Y-m-d h:i:s"),
				'bp_name' => BP::where('mobile',$bp_mobile."")
						->pluck('name'),						
				'lsa_code' => BP::where('mobile',$bp_mobile."")
						->pluck('lsa_code')

			]);

			$this->u1($msg);
		}


	}

	public function u1($msg){

			U1Message::create([

				'full_message'=>$msg->full_message,
				'campaign_id'=>$msg->campaign_id,
				'customer_name'=>$msg->customer_name?$msg->customer_name:"",
				'age'=>$msg->age?$msg->age:0,
				'gender'=>$msg->gender?$msg->gender:"",
				'customer_mobile'=>$msg->customer_mobile?$msg->customer_mobile:"",
				'area_code'=>$msg->area_code?$msg->area_code:0,
				'education_id'=>$msg->education_id?$msg->education_id:0,
				'occupation_id'=>$msg->occupation_id?$msg->occupation_id:0,
				'coupon_code'=>$msg->coupon_code?$msg->coupon_code:0,
				'currently_used_product'=>$msg->currently_used_product?$msg->currently_used_product:0,
				'sales'=>$msg->sales?$msg->sales:0,				
				'bp_mobile'=>$msg->bp_mobile?$msg->bp_mobile:'',
				'time'=>$msg->time?$msg->time:'',
				'FAL'=>$msg->FAL?$msg->FAL:0,
				'PDF'=>$msg->PDF?$msg->PDF:0,
				'PWB'=>$msg->PWB?$msg->PWB:0,
				'PNS'=>$msg->PNS?$msg->PNS:0,
				'PPC'=>$msg->PPC?$msg->PPC:0,
				'DBM'=>$msg->DBM?$msg->DBM:0,

				'thana' => $msg->thana?$msg->thana:'',
				'district' => $msg->district?$msg->district:'',
				'division' => $msg->division?$msg->division:'',

				'bp_name' => $msg->bp_name?$msg->bp_name:'',						
				'lsa_code' => $msg->lsa_code?$msg->lsa_code:'',

				'date' => $msg->date?$msg->date:'',

				'error'=>$msg->error?$msg->error:''

			]);

	}

	

}