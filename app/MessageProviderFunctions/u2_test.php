<?php

	function parse($message){		

		
		$msg_len = strlen($message);		

		$errors = null;

		if(substr($message, 0, 3) != 'u2 '){//there must be a space preceding proper campaign id

			$errors = 'Campaign id not valid';				
			
			return $errors;				

		}

		$pos = strpos($message, ' ');
						
		$message[$pos] = ',';//replacing the space after u2 with ,

		$message = explode(",",$message);//$message is an array of tokens


		if(sizeof($message) < 11){// 11th number field is yes/no

			$errors = "Insufficient number of fields";			

			return $errors;				
		}

		if($msg_len > 160){//if message length exceeding 

			$errors = "Message Length Exceeded";			

			return $errors;				

		}

		if(ucfirst($message[10]) == 'Y' && sizeof($items_sold) == 0){

			$errors = "Items sold fields blank";

			return $errors;				
			
		}
		

		if( !strlen($message[1]) ){//customer name

			$errors = "Customer name field blank";	

			return $errors;				

		}else{
			
				if( strlen($message[1]) > 15 ){

					$errors = "Customer name more than 15 characters";

					return $errors;				

				}

		}

		if(!strlen($message[2])){//age

			$errors = "Customer age field blank";

			return $errors;				

		}else{

			if( !ctype_digit($message[2]) ){

				$errors = "Customer age not numeric";

				return $errors;				

			}

			if( $message[2] < 10 || $message[2] > 100  ){

				$errors = "Customer age not in limit";

				return $errors;				

			}
		
		}




		if(!strlen($message[3])){//gender

			$errors = "Customer gender field is blank";

			return $errors;				

		}else{

			if($message[3] !== 'm' && $message[3] !=='f' && $message[3] !== 'M' && $message[3] !== 'F'){

				$errors = "Customer gender type undefined";

				return $errors;				
			}
			
		}

		if( !strlen($message[4]) ){//customer mobile

			$errors = "Customer mobile field blank";

			return $errors;				

		}else if( !preg_match("/^01[0-9]{9}$/", $message[4])){//changed here
			
			$errors = "Customer mobile invalid format";

			return $errors;				

		}else{

			if( strlen($message[4]) != 11 ){

				$errors = "Customer mobile field not 11 digit";

				return $errors;				
			}

		}

		if( strlen($message[5])!=4 ){//area code

			$errors = "Thana code invalid format";

			return $errors;				

		}


		if(!strlen($message[6])){//education

			$errors = "Education field blank";

			return $errors;				

		}else{

			if($message[6] < 1 || $message[6] > 5){

				$errors = "Education value not in range";

				return $errors;				

			}
		}



		if(!strlen($message[7])){//occupation

			$errors = "Occupation field blank";

			return $errors;				

		}else{

			if($message[7] < 1 || $message[7] > 4 ){

				$errors = "Occupation value not in range";

				return $errors;				

			}

		}


		if(!strlen($message[8])){//coupon

			$errors = "Coupon Code Blank";		

			return $errors;				

		}

		if(!strlen($message[9])){//currently using product

			$errors = "Currently Using Products Field blank";

			return $errors;						

		}else{

			if($message[9] < 1 || $message[9] > 6){

				$errors = "Currently Using Product Not Listed";		

				return $errors;					
	
			}
		
		}

		if(!strlen($message[10])){//sales yes or no

			$errors = "Sales Field Blank";

			return $errors;				

		}else{

			if($message[10] !='y' && $message[10] !='n' && $message[10] !='Y' && $message[10] !='N'){

				$errors = "Sales format invalid";

				return $errors;				
			}
		}


		return null;		

	}


	$message = "u2 hasan,30,f,01710340450,1001,5,1,91418709,2,y";
	
	$error = parse($message);

	if($error){
	
		var_dump($error);
		
	}else{
	
		var_dump("no error");
	}


?>