<?php

class TSalesArea extends MEloquent {
	
	protected $collection = 'target_sales_area';

	protected $guarded = [];

	protected $primary_key = '_id';

	public $timestamps = false;


}