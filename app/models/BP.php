<?php

class BP extends MEloquent {
	
	protected $collection = 'bp_info';

	protected $guarded = [];

	protected $primary_key = '_id';

	public $timestamps = false;


}