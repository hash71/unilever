<?php

class TSalesMonth extends MEloquent {
	
	protected $collection = 'target_sales_month';

	protected $guarded = [];

	protected $primary_key = '_id';

	public $timestamps = false;


}