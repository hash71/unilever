<?php

class Coupon extends MEloquent {
	
	protected $collection = 'coupons';

	protected $guarded = [];

	protected $primary_key = '_id';

	public $timestamps = false;


}