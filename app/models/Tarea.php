<?php

class Tarea extends MEloquent {
	
	protected $collection = 'target_area';

	protected $guarded = [];

	protected $primary_key = '_id';

	public $timestamps = false;


}