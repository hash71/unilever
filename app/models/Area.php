<?php

class Area extends MEloquent {
	
	protected $collection = 'area_info';

	protected $guarded = [];

	protected $primary_key = '_id';

	public $timestamps = false;


}